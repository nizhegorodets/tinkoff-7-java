import java.util.LinkedList;
import java.util.Random;

public class ClientImpl implements Client {
    @Override
    public Event readData() throws InterruptedException {
        Thread.sleep(1000);
        Random rnd = new Random();
        int start = 1;
        int end = 3;
        int adressesCount = rnd.nextInt((end - start) + 1) + start;
        LinkedList<Address> addresses = new LinkedList<>();
        for (int i = 0; i < adressesCount; i++) {
            addresses.add(new Address("1", String.valueOf(i)));
        }

        byte[] array = new byte[16];
        new Random().nextBytes(array);

        Payload payload = new Payload("origin1", array);
        return new Event(addresses, payload);
    }

    @Override
    public synchronized Result sendData(Address dest, Payload payload) {
        Random rnd = new Random();
        int start = 0;
        int end = 1;
        int randomAns = rnd.nextInt((end - start) + 1) + start;
        if (randomAns == 0) {
            return Result.REJECTED;
        }
        else {
            return Result.ACCEPTED;
        }
    }
}
