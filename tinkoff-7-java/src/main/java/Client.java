public interface Client {
    //блокирующий метод для чтения данных
    Event readData() throws InterruptedException;

    //блокирующий метод отправки данных
    Result sendData(Address dest, Payload payload);
}
