import java.util.Objects;

public record Address(String datacenter, String nodeId) {
    private static int hashCode;
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Address that = (Address) o;
        return Objects.equals(datacenter, that.datacenter) && Objects.equals(nodeId, that.nodeId);
    }

    @Override
    public int hashCode() {
        return this.hashCode;
    }
}
