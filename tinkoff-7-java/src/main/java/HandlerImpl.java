import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class HandlerImpl implements Handler {
    private final HashMap<Address, BlockingQueue<Payload>> storage = new HashMap<>();
    private final Duration timeout = Duration.of(500, ChronoUnit.MILLIS);
    @Override
    public Duration timeout() {
        return timeout;
    }

    @Override
    public void performOperation() throws InterruptedException {
        ExecutorService pool = Executors.newCachedThreadPool();
        // infinite reading start (new events)
        Client client = new ClientImpl();
        while (true) {
            Event newEvent = client.readData();

            List<Address> recipients = newEvent.recipients();
            for (Address address : recipients) {
                if (!storage.containsKey(address)) {
                    System.out.printf("address %s is not contain in the storage%n", address);
                    addEmptyList(address);
                    BlockingQueue<Payload> queue = storage.get(address);
                    queue.put(newEvent.payload());
                    pool.execute(() -> {
                        // infinite reading start (new payloads)
                        while (true) {
                            Payload newPayload = null;
                            try {
                                newPayload = queue.take();
                            } catch (InterruptedException e) {
                                System.out.println("error while take a new payload from queue%n");
                            }

                            Result resp = client.sendData(address, newPayload);
                            while (Result.REJECTED == resp) {
                                System.out.printf("payload %s was rejected for %s address. retrying...%n", newPayload, address);
                                try {
                                    Thread.sleep(timeout().toSeconds());
                                } catch (InterruptedException e) {
                                    System.out.println("exception while sleeping%n");
                                }
                                resp = client.sendData(address, newPayload);
                            }
                            System.out.printf("payload %s was sent to %s adress%n", newPayload, address);
                        }
                    });
                } else {
                    BlockingQueue<Payload> queue = storage.get(address);
                    queue.put(newEvent.payload());
                }
            }
        }
    }

    private synchronized void addEmptyList(Address address) {
        storage.put(address, new LinkedBlockingQueue<>());
    }
}
